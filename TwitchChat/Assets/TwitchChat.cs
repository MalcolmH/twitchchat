﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;

public class TwitchChat : MonoBehaviour
{
    public Text chat;
    private string txtpath;
    private StreamReader sr;

    // Use this for initialization
    void Start ()
    {
        txtpath = Application.dataPath + "/probabilities.txt";
        OpenFileName ofn = new OpenFileName(); //Open file dialog
        ofn.structSize = Marshal.SizeOf(ofn);
        ofn.filter = "All Files\0*.*\0\0";
        ofn.file = new string(new char[256]);
        ofn.maxFile = ofn.file.Length;
        ofn.fileTitle = new string(new char[64]);
        ofn.maxFileTitle = ofn.fileTitle.Length;
        ofn.initialDir = UnityEngine.Application.dataPath;
        ofn.title = "Load chat log file";
        ofn.defExt = "txt";
        ofn.flags = 0x00080000 | 0x00001000 | 0x00000800 | 0x00000200 | 0x00000008; //Flags: OFN_EXPLORER|OFN_FILEMUSTEXIST|OFN_PATHMUSTEXIST| OFN_ALLOWMULTISELECT|OFN_NOCHANGEDIR
        if (OpenDialogDLL.GetOpenFileName(ofn))
            txtpath = ofn.file;
    }
    
    // Update is called once per frame
    void Update ()
    {
        sr = new StreamReader(new FileStream(txtpath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
        chat.text = sr.ReadToEnd();
        sr.Close();
    }
}
